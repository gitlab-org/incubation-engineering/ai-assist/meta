# Dockerfile linters

## Tl;DR;

Desk research on existing Dockerfile linters. Hadolint seems to be the only viable option.

## Linters tested

Based on Googling on "dockerfile linter" below came up as unique linters on the first 10 pages.


| Project | Stars | Commits | Last release | State       | URL |
| ------- | ----- |----|-----------|-------------| --- |
| Hadolint | 6.7k | 1060 | 31-03-2022 | Very active | https://github.com/hadolint/hadolint |
|Dockerlint | 177 | 154 | 10-01-2019 | Abandonned  | https://github.com/RedCoolBeans/dockerlint |
| dockerfile-linter | 24 | 40 |  6-01-2022 | Active      | https://github.com/buddy-works/dockerfile-linter |
| whalelint | 9 | 42 | 4-01-2022 | Active      | https://github.com/CreMindES/whalelint | 

I was only familiar with Hadolint, judging by the number of stars and releases it seems logical as it is by far the most
popular Dockerfile linter. Regardless lets test all of them, so we have a better comparison.

## Test case
The Dockerfile in this repo has been rigged with a few suboptimal practices and will be linted by all the above
linters.

### Hadolint

```bash
brew install hadolint
hadolint Dockerfile

# Output:
Dockerfile:6 DL3020 error: Use COPY instead of ADD for files and folders
Dockerfile:9 DL3042 warning: Avoid use of cache directory with pip. Use `pip install --no-cache-dir <package>`
Dockerfile:12 DL3020 error: Use COPY instead of ADD for files and folders
Dockerfile:18 DL3025 warning: Use arguments JSON notation for CMD and ENTRYPOINT arguments
```

#### Conclusion

As I am familiar with Hadolint, I suspected exactly these errors in my Dockerfile.

### Dockerfilelint

```bash
docker run -it --rm -v $(pwd)/Dockerfile:/Dockerfile:ro redcoolbeans/dockerlint

# Output:
WARN:  Recommended exec/array form not used on line 18
WARN:  ADD instruction used instead of COPY on line 6, 12

INFO: /Dockerfile is OK.
```

#### Conclusion

Very similar result to Hadolint, but here we see a warning for line 6 and 12 instead of an error. The `--no-cache` was
not detected which would have resulted in a smaller image. Interestingly Hadolint mentions that this linter has also
implemented rule [DL3020](https://github.com/hadolint/hadolint/wiki/DL3020).


### Dockerfile-linter

```bash
git clone git@github.com:buddy-works/dockerfile-linter.git
cd dockerfile-linter
docker build . -t dockerfile-linter
docker run -v $(pwd)/Dockerfile:/dockerfilelinter/dockerfile dockerfile-linter linter -f dockerfile

# Output
SyntaxError: Unexpected end of JSON input
    at JSON.parse (<anonymous>)
    at promiseList.push.exec.then (/dockerfilelinter/lib/shellcheck.js:33:38)
Line 6:
   /EA0002/ Use COPY instead of ADD for files and folders
Line 12:
   /EA0002/ Use COPY instead of ADD for files and folders
Line 18:
   /EJ0002/ CMD and ENTRYPOINT should be written in JSON form

```

#### Conclusion

Not sure if the results are skewed by the error. Output seems to be identical to `Dockerfilelint`.


### Whalelint

```bash
# 3 month old image
docker run --rm -v $(pwd)/Dockerfile:/Dockerfile ghcr.io/cremindes/whalelint:bd9279e Dockerfile

# Output:
# Unfortunately it did not work, was frozen for 4 minutes before I stopped it.

# 1 year old image
docker run --rm -it -v $(pwd)/Dockerfile:/Dockerfile cremindes/whalelint:latest Dockerfile

# Output:
DEBU[0000] We have27ruleset.                            
INFO[0000] Running linter... TODO &{summary false [Dockerfile] app normal} 
ERRO[0000] Unhandled Command!                           
ERRO[0000] Unhandled Command!                           
ERRO[0000] Unhandled Command!                           
DEBU[0000] Cannot convert port string to int!           
WhaleLint summary: 1 Error, 1 Warning

Error:
Line 15 | EXP001 | Expose a valid UNIX port.

Warning:
Line 18 | CMD001 | Prefer JSON notation array format for CMD and ENTRYPOINT
```

#### Conclusion

The older image returned some results, `EXP001` is interesting, I'm not sure if the usage of `ENV` leads to the storing
of a string, regardless it works for passing along to uvicorn. I reran the Hadolint with `EXPOSE 100000` and it reported

```commandline
DL3011 error: Valid UNIX ports range from 0 to 65535
```

So it might be that Whalelint does not parse the rendering of variables correctly.


## Conclusion

`Hadolint` seems to be the most popular linter for Dockerfiles, it also had the smoothest experience, no errors and clear
output with links to actual code. `Dockerfilelint` seemed a good alternative but its no longer maintained and (therefor)
not as complete as `hadolint`.

