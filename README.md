# AI Assist meta

This project is a container for all documents supporting AI assist, non-code related issues, etc.


## Important links

Handbook: https://about.gitlab.com/handbook/engineering/incubation/ai-assist/
Weekly summary: https://gitlab.com/gitlab-org/incubation-engineering/ai-assist/meta/-/issues/1
YouTube: https://www.youtube.com/playlist?list=PL05JrBw4t0KoQ2a8sLO059BdU4F5c-jGE


## How to contribute

Contributions are very welcome, whether its feedback, an idea, anything.

- Star this project to keep notified of weekly releases
- Open an issue describing your ideas / suggestions
- Comment on issues, merge requests 
- Slack
